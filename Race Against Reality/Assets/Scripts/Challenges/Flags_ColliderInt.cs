﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flags_ColliderInt : Flags
{
    [SerializeField] protected int RequiredCount = 5;
    [SerializeField] internal int CurrentAmount = 0;
    [SerializeField] internal EntityProperty RequiredProperty;

    public override bool FlagCheck()
    {
        if (CurrentAmount >= RequiredCount)
        {
            isDone = true;
            return true;
        }
        else
            return false;
    }

    [ContextMenu("Add Point")]
    public void AddPoint()
    {
        CurrentAmount++;
    }

    public override void Reset()
    {
        base.Reset();
        CurrentAmount = 0;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<EntityItem>().HasProperty(RequiredProperty.Property))
        {
            AddPoint();
        }
    }
}
