﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Challenge_Blocker : Challenge
{
    [SerializeField] GameObject WinPanel, LosePanel;
    [SerializeField] GameObject Ball;
    [SerializeField] Rigidbody ballRB;
    [SerializeField] Vector3 initialPosition;
    [SerializeField] float ballSpeed;
    [SerializeField] float TimeTilWin;
    float currCountdownValue;
    [SerializeField] Slider timer;
    bool MoveMe;

    protected override void Start()
    {
        base.Start();
        ballRB = Ball.GetComponent<Rigidbody>();
        StartCoroutine(StartCountdown());
    }

    private void Update()
    {
        if (MoveMe) Move();

    }

    void Move()
    {

        ballRB.AddForce(Vector3.right * ballSpeed, ForceMode.Acceleration);


    }

    public IEnumerator StartCountdown()
    {
        currCountdownValue = TimeTilWin;
        while (currCountdownValue > 0)
        {
            Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
            timer.value = currCountdownValue;
        }
    }
}