﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flags_DebugInt : Flags
{
    [SerializeField] protected int RequiredCount = 5;
    [SerializeField] protected int CurrentAmount = 0;

    public override bool FlagCheck()
    {
        if (CurrentAmount >= RequiredCount)
        {
            isDone = true;
            return true;
        }
        else
            return false;
    }

    [ContextMenu("Add Point")]
    public void AddPoint()
    {
        CurrentAmount++;
    }

    public override void Reset()
    {
        base.Reset();
        CurrentAmount = 0;
    }
}
