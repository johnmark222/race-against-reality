﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Challenge_Platforms : Challenge
{

    [SerializeField] GameObject WinPanel, LosePanel;

    [SerializeField]
    GameObject[] Movers;
    [SerializeField]
    List<Vector3> moversInitialPosition;
    [SerializeField]
    List<Quaternion> moversInitialRotation;
    [SerializeField]
    List<Rigidbody> moversRB;
    [SerializeField]
    internal float moverSpeed = 10;
    [SerializeField] internal float minX, maxX, minY, maxY;
    [SerializeField] bool willMoveItems;
    [SerializeField] Vector3 forceDirection;
    [SerializeField] Text counterScoreText, CountdownReference;
    Flags_ColliderInt myCounter;
    [SerializeField] internal float currentSpeed;
    [SerializeField] internal List<Target_EnableFollow> followScripts;
    [SerializeField] Button PlayButton, RestartButton;

    protected override void Start()
    {
        base.Start();

        //StartGame();
    }

    public override void Initialize()
    {
        base.Initialize();
        myCounter = Objectives[0].currentFlags[0].GetComponent<Flags_ColliderInt>();
        currentSpeed = moverSpeed;
        foreach (GameObject mover in Movers)
        {
            if (mover.GetComponent<Rigidbody>())
            {
                moversRB.Add(mover.GetComponent<Rigidbody>());
                moversInitialPosition.Add(mover.transform.position);
                moversInitialRotation.Add(mover.transform.rotation);
            }
        }

        foreach (Target_EnableFollow follower in followScripts)
        {
            follower.IsFollowing = true;
        }
    }

    void StartGame()
    {
        willMoveItems = true;

        foreach(Target_EnableFollow follower in followScripts)
        {
            follower.IsFollowing = false;
        }
       
    }
    public void StartMe()
    {
        StartCoroutine(Count());
        PlayButton.gameObject.SetActive(false);
        RestartButton.gameObject.SetActive(false);
    }
    IEnumerator Count()
    {
        CountdownReference.text = "3";
        yield return new WaitForSecondsRealtime(1);
        CountdownReference.text = "2";

        yield return new WaitForSecondsRealtime(1);
        CountdownReference.text = "1";

        yield return new WaitForSecondsRealtime(1);

        CountdownReference.text = " ";
        RestartButton.gameObject.SetActive(true);

        StartGame();

    }

    public void ResetGame()
    {
        willMoveItems = false;
        foreach (Target_EnableFollow follower in followScripts)
        {
            follower.IsFollowing = true;
        }
        for (int i = 0; i < Movers.Length; i++)
        {
            Movers[i].SetActive(true);
            moversRB[i].velocity = Vector3.zero;
            Movers[i].transform.position = moversInitialPosition[i];
            Movers[i].transform.rotation = moversInitialRotation[i];
        }

        PlayButton.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        Process();
		if(willMoveItems)
        {
            foreach(Rigidbody rb in moversRB)
            {
                rb.AddForce(forceDirection * currentSpeed,ForceMode.Acceleration);
                if(counterScoreText)UpdateScore();

                if(!IsAllMoversAlive())
                {

                    Lose();
                }
            }
        }
	}

    public bool IsAllMoversAlive()
    {
        bool IsAlive = true;

        foreach(GameObject mover in Movers)
        {
            if(!mover.activeSelf)
            {
                IsAlive = false;
                
            }
        }
        return IsAlive;
    }

    public override void EndGame()
    {
        //base.EndGame();
        WinGame();
    }

    public void CompleteGame()
    {
        if (GameManager.Instance) GameManager.Instance.FinishChallenge();

        CurrentState = ChallengeState.DONE;
        if (GameManager.Instance) GameManager.CurrentChallenge = null;
    }

    public void Lose()
    {
        LoseGame();
    }

    void UpdateScore()
    {
        counterScoreText.text = myCounter.ToString();
    }

    void WinGame()
    {
        ResetGame();
        WinPanel.SetActive(true);
    }

    void LoseGame()
    {
        ResetGame();
        LosePanel.SetActive(true);
    }
}
