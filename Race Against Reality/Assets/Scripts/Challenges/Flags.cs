﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Flags : MonoBehaviour 
{
	[SerializeField]
	internal bool isDone;
	protected virtual void Start(){ isDone = false; }
	public abstract bool FlagCheck();

    public T CurrentFlag<T>(){ return this.gameObject.GetComponent<T>(); }

    public virtual void Reset()
    {
        isDone = false;
    }
}
