﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Challenge_PM3 : Challenge
{
    [SerializeField]
    Objective EnemyObjective;

    [SerializeField]
    int ScoreReferenceP, ScoreReferenceE;

    [SerializeField]
    Text ScoreTextP, ScoreTextE, CountdownReference, WinMessage;
    [SerializeField]
    GameObject WinPanel, LosePanel;

    [SerializeField]
    GameObject Ball, EnemyPaddle;
    Vector3 BallInitialPosition;
    Rigidbody ePaddleRB;
    [SerializeField]
    float ePaddleSpeed = 10;
    [SerializeField]
    float eComputation;
    [SerializeField] internal float minX, maxX;
    [SerializeField]
    Vector3 InitialBallPosition;
    
    Flags_ColliderInt EnemyFlag;
    Flags_ColliderInt PlayerFlag;

    protected override void Start()
    {
        base.Start();
        ePaddleRB = EnemyPaddle.GetComponent<Rigidbody>();
        BallInitialPosition = Ball.transform.position;
    }

    public override void Initialize()
    {
        base.Initialize();

        EnemyFlag = Objectives[0].currentFlags[0].CurrentFlag<Flags_ColliderInt>();

        PlayerFlag = Objectives[0].currentFlags[0].CurrentFlag<Flags_ColliderInt>();

#if UNITY_EDITOR
        //StartGame();
#endif
    }

    private void Update()
    {

        eComputation = Ball.transform.position.x - EnemyPaddle.transform.position.x;
        if (eComputation > 0 && EnemyPaddle.transform.position.x < maxX)
        {
            ePaddleRB.velocity = new Vector3(ePaddleSpeed, ePaddleRB.velocity.y);

        }
        else if (eComputation < 0 && EnemyPaddle.transform.position.x > minX)
        {
            ePaddleRB.velocity = new Vector3(-ePaddleSpeed, ePaddleRB.velocity.y);
        }
        else
            ePaddleRB.velocity = Vector3.zero;

        Flags_ColliderInt a = EnemyObjective.currentFlags[0].GetComponent<Flags_ColliderInt>();
        if (a.CurrentAmount != ScoreReferenceP)
        {
            ScoreReferenceP = a.CurrentAmount;
            UpdateScore();
        }
        Flags_ColliderInt b = Objectives[0].currentFlags[0].GetComponent<Flags_ColliderInt>();
        if (b.CurrentAmount != ScoreReferenceE)
        {
            ScoreReferenceE = b.CurrentAmount;
            UpdateScore();
        }
        

    }

    void UpdateScore()
    {
        ScoreTextE.text = ScoreReferenceP.ToString();
        ScoreTextP.text = ScoreReferenceE.ToString();
        if (ScoreReferenceE >= 3 && ScoreReferenceP < 3)
        {
            // Enemy Wins
            WinGame();
            return;
        }

        if (ScoreReferenceP >= 3 && ScoreReferenceE < 3)
        {
            // Player wins
            
            LoseGame();
            return;
        }

        ResetGame();
    }

    void StartGame()
    {
        int rando = Random.Range(-1, 1);
        if (rando > 0)
        {

            Ball.GetComponent<Rigidbody>().velocity = Vector3.up * 10;
        }
        else
        {
            Ball.GetComponent<Rigidbody>().velocity = -Vector3.up * 10;

        }
    }
    public void StartMe()
    {
        StartCoroutine(Count());
    }
    IEnumerator Count()
    {
        CountdownReference.text = "3";
        yield return new WaitForSecondsRealtime(1);
        CountdownReference.text = "2";

        yield return new WaitForSecondsRealtime(1);
        CountdownReference.text = "1";

        yield return new WaitForSecondsRealtime(1);

        CountdownReference.text = " ";

        StartGame();

    }

    void ResetGame()
    {
        Ball.transform.position = BallInitialPosition;
        Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        StartMe();
    }

    void RestartGame()
    {
        Flags_ColliderInt a = EnemyObjective.currentFlags[0].GetComponent<Flags_ColliderInt>();
        a.CurrentAmount = 0;
        Flags_ColliderInt b = Objectives[0].currentFlags[0].GetComponent<Flags_ColliderInt>();
        b.CurrentAmount = 0;
        ResetGame();
    }

    public void Win()
    {
        Flags_ColliderInt b = Objectives[0].currentFlags[0].GetComponent<Flags_ColliderInt>();

        ChallengeObjectAssociated.ChallengeScore = b.CurrentAmount;
        EndGame();
    }

    public void Lose()
    {
        RestartGame();
    }

    void WinGame()
    {
        Ball.transform.position = BallInitialPosition;
        Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        Flags_ColliderInt a = EnemyObjective.currentFlags[0].GetComponent<Flags_ColliderInt>();
        Flags_ColliderInt b = Objectives[0].currentFlags[0].GetComponent<Flags_ColliderInt>();
        WinMessage.text = "You have WON WITH THE SCORE OF: " + b.CurrentAmount + " - " + a.CurrentAmount;
        WinPanel.SetActive(true);
    }

    void LoseGame()
    {
        Ball.transform.position = BallInitialPosition;
        Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        LosePanel.SetActive(true);
    }


}
