﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChallengeState{NOTDONE, INPROGRESS, DONE}

public class Challenge : MonoBehaviour
{
    internal static Challenge instance;

    [SerializeField]
    internal ChallengeObject ChallengeObjectAssociated;
	[SerializeField]
	internal Objective[] Objectives;
	[SerializeField]
	internal int CurrentObjectiveIndex;
    [SerializeField]
	protected ChallengeState CurrentState;
	public ChallengeState CurrentChallengeState { get { return CurrentState; } }

    public static T CurrentChallenge<T>()
    {
        return instance.GetComponent<T>();
    }

	protected virtual void Start() 
	{
		Objectives = GetComponents<Objective>();

        if (instance == null)
            instance = this;
        else
            Destroy(this);

        Initialize();
	}
	public virtual void Initialize()
	{
        if (GameManager.Instance) GameManager.CurrentChallenge = this;
		foreach(Objective objective in Objectives)
		{
			objective.Reset();
		}

		CurrentState = ChallengeState.INPROGRESS;
		gameObject.SetActive(true);
		CurrentObjectiveIndex = 0;
		// TODO: Play sound here on challenge start

		// TODO: UI Initialize using this

		Objectives[CurrentObjectiveIndex].Initialize();
		// TODO: Assign Audio Button Listener here
		// TODO: UI Switch Screen
	}

	public virtual void Process()
	{
		if(CurrentState == ChallengeState.DONE) return;


        if (Objectives.Length < 0) return;

        Objectives[CurrentObjectiveIndex].Process();
		if(Objectives[CurrentObjectiveIndex].ObjectiveCheck)
		{
			Objectives[CurrentObjectiveIndex].End();
			if(CurrentObjectiveIndex == Objectives.Length - 1)
			{
                if (GameManager.Instance) GameManager.Warning("Challenge Finished", "red");
				EndGame();
			}
			else
			{
                if (GameManager.Instance) GameManager.Warning("Moving to next objective", "blue");

                ActivateNextObjective();
			}
		}
	}
	public virtual void ForceEnd()
	{
        if (GameManager.Instance) GameManager.Instance.FinishChallenge();
        Objectives[CurrentObjectiveIndex].End();
		CurrentState = ChallengeState.DONE;
        
	}
	public virtual void EndGame()
	{
        if (GameManager.Instance) GameManager.Instance.FinishChallenge();

        CurrentState = ChallengeState.DONE;
        if(GameManager.Instance) GameManager.CurrentChallenge = null;

    }

    public virtual void ActivateNextObjective()
	{
		CurrentObjectiveIndex++;
		// TODO: Change the UI to game screen relative to the objective
		Objectives[CurrentObjectiveIndex].Initialize();
		
	}
	
}
