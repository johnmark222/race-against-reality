﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Challenge_IfOnlyIMoved : Challenge
{
    [SerializeField] GameObject WinPanel, LosePanel;
    [SerializeField] internal Vector3 initialPosition;
    [SerializeField] internal GameObject mover;
    internal Rigidbody moverRB;
    [SerializeField] internal float moveSpeed = 1;
    [SerializeField] internal bool willMoveItems = false;
    [SerializeField] Vector3 moveVector;
    bool IsMove = false;
    protected override void Start()
    {
        base.Start();
        moverRB = mover.GetComponent<Rigidbody>();
        initialPosition = mover.transform.position;
    }

    public void StartMe()
    {
        willMoveItems = true;
    }

    public void ResetGame()
    {
        willMoveItems = false;
        mover.transform.position = initialPosition;

    }

    private void FixedUpdate()
    {
        Process();
        if(IsMove)
        {
            moverRB.AddForce(moveVector * moveSpeed, ForceMode.Acceleration);
        }
#if UNITY_EDITOR
        moverRB.AddForce(new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * moveSpeed, ForceMode.Acceleration);
#endif
    }

    public override void EndGame()
    {
        //base.EndGame();
        WinGame();
    }

    public void WinGame()
    {
        WinPanel.SetActive(true);
    }

    public void CompleteGame()
    {
        if (GameManager.Instance) GameManager.Instance.FinishChallenge();

        CurrentState = ChallengeState.DONE;
        if (GameManager.Instance) GameManager.CurrentChallenge = null;
    }

    public void MoveUp()
    {
        //moverRB.AddForce(Vector3.up * moveSpeed, ForceMode.Acceleration);
        moveVector = Vector3.up;
        IsMove = true;
    }
    public void MoveLeft()
    {
        moveVector = Vector3.left;
        IsMove = true;
    }
    public void MoveRight()
    {
        moveVector = Vector3.right;
        IsMove = true;
    }
    public void MoveDown()
    {
        moveVector = Vector3.down;
        IsMove = true;
    }
    public void DontMove()
    {
        IsMove = false;
        moveVector = Vector3.zero;
    }
}
