﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Objective : MonoBehaviour 
{
	public string ObjectiveDescription;
	[SerializeField] protected List<GameObject> ObjectssToActivate;
	[SerializeField] protected List<Flags> Flags;
    public List<Flags> currentFlags { get { return Flags; } }

    protected List<GameObject> ConcernedItems;

    protected virtual void Start() { }

	public abstract void Process();

    public virtual void Initialize()
	{
        Reset();
		// Default initialize
		foreach(GameObject gO in ObjectssToActivate)
		{
			gO.gameObject.SetActive(true);
		}
	}

    public virtual void End()
	{
		// Default initialize
		foreach(GameObject gO in ObjectssToActivate)
		{
			gO.gameObject.SetActive(false);
		}

        //Reset();
	}

	public virtual void Reset()
    {
        
        foreach (Flags f in Flags)
        {
            f.Reset();
        }
    }

    public virtual void ForceEnd()
	{
		End();
		// TODO:  For instances that will make the challenge just say true
	}

	public bool ObjectiveCheck
	{
		get
		{
			for (int i = 0; i < Flags.Count; i++)
				if (!Flags[i].FlagCheck()) return false;
			return true;		
		}
	}

}
