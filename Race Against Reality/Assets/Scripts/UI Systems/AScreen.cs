﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(Animator))]
public class AScreen : MonoBehaviour
{
    protected CanvasGroup canvasGroup;
    protected Animator screenAnimator;
    protected AudioClip clipOnEnable;

	private void Awake()
    {
		if(!canvasGroup)canvasGroup = GetComponent<CanvasGroup>();
		if(!screenAnimator) screenAnimator = GetComponent<Animator>();

	}

	protected virtual void Start()
	{

    }

	protected virtual void Update()
	{

    }

	public void Deactivate()
	{
		canvasGroup.interactable = false;
		GameManager.Warning("Deactivating " + gameObject.name, "blue");
		screenAnimator.Play("deactivate");
		this.gameObject.SetActive(false);
	}
	
	public void Activate()
	{
		this.gameObject.SetActive(true);
		GameManager.Warning("Activating " +gameObject.name, "blue");
		// TODO: Play Sound Here
		screenAnimator.Play("activate");
	}
}
