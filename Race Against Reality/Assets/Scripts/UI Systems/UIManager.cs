﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour 
{
	public static UIManager Instance;
	AScreen currentScreen;

	public static AScreen CurrentScreen
	{
		get
		{
			return Instance.currentScreen;
		}
	}

	public AScreen previousScreen;
	
	public static AScreen PreviousScreen
	{
		get
		{
			if(!Instance.previousScreen)
				return Instance.previousScreen;
			else
				return null;
		}
	}

	private void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	public static T GetScreen<T>()
	{
		return Instance.GetComponentInChildren<T>(true);
	}

	public static void SwitchScreen(AScreen newScreen)
	{
		// TODO: Do stuff if lost tracking

		GameManager.Warning("Loading New Screen", "blue");
		if(Instance.currentScreen != null) Instance.currentScreen.Deactivate();
		Instance.currentScreen = newScreen;
		Instance.currentScreen.Activate();
	}
}
