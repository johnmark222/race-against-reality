﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEntity : EntityItem 
{
	protected Image CurrentImage;
	protected CanvasGroup CurrentCanvasGroup;
	protected Animator CurrentAnimator;
    protected Coroutine CurrentAlphaChange;

	public override void Initialize()
	{
		base.Initialize();

		if(GetComponent<Image>()) CurrentImage = GetComponent<Image>();
		if(GetComponent<CanvasGroup>()) CurrentCanvasGroup = GetComponent<CanvasGroup>();
		if(GetComponent<Animator>()) CurrentAnimator = GetComponent<Animator>();
		Disable();
	}

	public virtual void Enable()
	{
		if(CurrentCanvasGroup) CurrentCanvasGroup.alpha = 1;
		else
		{
			Color tempColor = CurrentImage.color;
			tempColor.a = 1;
			CurrentImage.color = tempColor;
		}
	}

	public virtual void Disable()
	{
		if(CurrentCanvasGroup) CurrentCanvasGroup.alpha = 0;
		else
		{
			Color tempColor = CurrentImage.color;
			tempColor.a = 0;
			CurrentImage.color = tempColor;
		}
	}

	public virtual void Activate()
	{
		this.gameObject.SetActive(true);
		if(CurrentCanvasGroup) CurrentCanvasGroup.interactable = true;
		else CurrentImage.raycastTarget = true;

		if(CurrentAnimator) ActivateAnimation();
		else ActivateUsingFadeEffect();
	}

	public virtual void Deactivate()
	{
		if(CurrentCanvasGroup) CurrentCanvasGroup.interactable = false;
		else CurrentImage.raycastTarget = false;

		if(CurrentAnimator) DeactivateAnimation();
		else DeactivateUsingFadeEffect();
	}

    public void DeactivateAnimation()
    {
        CurrentAnimator.Play("exit_transition");
        this.gameObject.SetActive(false);
    }

    public void ActivateAnimation()
    {
        CurrentAnimator.Play("enter_transition");
    }

	protected virtual void ActivateUsingFadeEffect()
	{
		if(CurrentCanvasGroup)
		{
			if (CurrentAlphaChange != null) StopCoroutine(CurrentAlphaChange);
			CurrentAlphaChange = StartCoroutine(FadeEffectCanvasGroup(1)); 
		}
		else
		{
			if (CurrentAlphaChange != null) StopCoroutine(CurrentAlphaChange);
			CurrentAlphaChange = StartCoroutine(FadeEffectImage(1)); 			
		}     
	}

	protected virtual void DeactivateUsingFadeEffect()
	{
		if(CurrentCanvasGroup)
		{
			if (CurrentAlphaChange != null) StopCoroutine(CurrentAlphaChange);
			CurrentAlphaChange =  StartCoroutine(FadeEffectCanvasGroup(0, () => {
				this.gameObject.SetActive(false);
			}));
		}
		else
		{
			if (CurrentAlphaChange != null) StopCoroutine(CurrentAlphaChange);
			CurrentAlphaChange =  StartCoroutine(FadeEffectImage(0, () => {
				this.gameObject.SetActive(false);
			}));			
		}
	}

	protected virtual IEnumerator FadeEffectImage(float alphaTarget, Action callBack= null)
	{
		float delta = alphaTarget - CurrentImage.color.a;
        while (Mathf.Abs(delta)>0.01f) 
		{
			float colorA = CurrentImage.color.a;
            colorA += delta * 0.2f;
            CurrentImage.color = new Color(CurrentImage.color.r, CurrentImage.color.g, CurrentImage.color.b, colorA);

			delta = alphaTarget -  CurrentImage.color.a;

            yield return new WaitForSeconds(0.1f);
        }

        CurrentImage.color = new Color(CurrentImage.color.r, CurrentImage.color.g, CurrentImage.color.b, alphaTarget);
        if (callBack != null) callBack();		
	}

	IEnumerator FadeEffectCanvasGroup(float alphaTarget, Action callBack= null)
	{
		float delta = alphaTarget - CurrentCanvasGroup.alpha;
        while (Mathf.Abs(delta)>0.01f) 
		{

            CurrentCanvasGroup.alpha += delta * 0.2f;//Mathf.Sign(delta)* 0.01f;
            delta = alphaTarget - CurrentCanvasGroup.alpha;
            yield return new WaitForSeconds(0.1f);
        }
        CurrentCanvasGroup.alpha = alphaTarget;
        if (callBack != null) callBack();
	}
}
