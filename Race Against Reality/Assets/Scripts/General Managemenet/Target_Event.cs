﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

[RequireComponent(typeof(ImageTargetBehaviour))]
[RequireComponent(typeof(BaseCustomTracker))]
public class Target_Event : TargetEntity
{
    [SerializeField]
    internal UnityEvent TrackEvent, UntrackEvent;
    public override void Initialize()
    {
        base.Initialize();
        currentTargetType = TargetType.ITEM;
        currentCustomTrackerBehavior.OnTrackingFoundEvent.AddListener(() => { TrackEvent.Invoke(); });
        currentCustomTrackerBehavior.OnTrackingFoundEvent.AddListener(() => { UntrackEvent.Invoke(); });

    }

}
