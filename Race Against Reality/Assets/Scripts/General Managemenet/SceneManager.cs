﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneLoader : MonoBehaviour
{

	public static SceneLoader Instance;
    public float SceneProgressOnLoad;
    public string CurrentChallengeString;
    public string ChallengeHomeString;
	// Use this for initialization
	void Awake () {
		if(Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}

        DontDestroyOnLoad(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void LoadGameScene(ChallengeObject currentChallengeToBeLoaded)
	{
		// Check if the scene has been already loaded

		// Loads scene if accepetable parameter
		SceneManager.LoadScene(currentChallengeToBeLoaded.ChallengName);
	}

    public static IEnumerator LoadSceneAsync(string SceneName, Action callback)
    {
        AsyncOperation aSyncLoad = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Single);

        while(!aSyncLoad.isDone)
        {
            Instance.SceneProgressOnLoad = aSyncLoad.progress;
            yield return null;
        }

        callback();
    }

    public static IEnumerator LoadSceneAsync(ChallengeObject currentChallengeToBeLoaded, Action callback)
    {
        AsyncOperation aSyncLoad = SceneManager.LoadSceneAsync(currentChallengeToBeLoaded.ChallengName, LoadSceneMode.Single);

        while (!aSyncLoad.isDone)
        {
            //Instance.SceneProgressOnLoad = aSyncLoad.progress;
            yield return null;
        }

        callback();

    }

    public static IEnumerator UnloadScene(ChallengeObject currentChallengeToBeLoaded, Action callback)
    {
        AsyncOperation aSyncHome = SceneManager.LoadSceneAsync(Instance.ChallengeHomeString);
        AsyncOperation aSyncLoad = SceneManager.UnloadSceneAsync(currentChallengeToBeLoaded.ChallengName);
        while (!aSyncLoad.isDone)
        {
            //Instance.SceneProgressOnLoad = aSyncLoad.progress;
            yield return null;
        }
        callback();
    }
}
