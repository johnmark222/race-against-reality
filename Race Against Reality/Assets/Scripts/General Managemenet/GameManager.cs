﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    FINDINGTARGET,
    WORLDVIEW,
    GAMEPLAY
}

public class GameManager : MonoBehaviour 
{
	public static GameManager Instance;
    [SerializeField] Challenge currentChallenge;

    public GameObject ChallengeTargetParent;

    public static Challenge CurrentChallenge
    {
        get
        {
            return Instance.currentChallenge;
        }
        internal set
        {
            Instance.currentChallenge = value;
        }
    }

    [SerializeField]
    private GameState currentState;
    public static GameState CurrentState
    {
        get { return Instance.currentState; }
        set { Instance.currentState = value; }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start () {
        currentState = GameState.FINDINGTARGET;
        // TODO: Switch Screen Game
        
	}

    private void Update()
    {
        
        switch (GameManager.CurrentState)
        {
            case GameState.WORLDVIEW:
                // TODO: Switch Screen
                break;
            case GameState.GAMEPLAY:
                // TODO: Switch Screen

                // Update Game
                if(CurrentChallenge)CurrentChallenge.Process();
                break;
            case GameState.FINDINGTARGET:
                // TODO: Switch Screen
                break;
            default:
                GameManager.Warning("This state in the game manager is not possible", "red");
                break;
        }
    }

    public static void LoadPreviousState()
    {
        if(GameManager.CurrentState != GameState.FINDINGTARGET)
        {
            switch(GameManager.CurrentState)
            {
                case GameState.WORLDVIEW:
                    // TODO: Switch Screen
                    break;
                case GameState.GAMEPLAY:
                    // TODO: Switch Screen

                    // Update Game
                    break;
                case GameState.FINDINGTARGET:
                    // TODO: Switch Screen
                    break;
                default:
                    GameManager.Warning("This state in the game manager is not possible", "red");
                    break;
            }
        }
    }

    public void FinishChallenge()
    {
        
        StartCoroutine(SceneLoader.UnloadScene(CurrentChallenge.ChallengeObjectAssociated, () =>
        {
            Instance.currentState = GameState.FINDINGTARGET;
            GameManager.Warning("Finished Challenge now going back to Main", "blue");
            GameManager.CurrentChallenge = null;
            foreach (Transform child in Instance.ChallengeTargetParent.transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        ));
    }

    public void LoadChallenge(ChallengeObject challengeAssociated)
    {
        if(GameManager.CurrentState == GameState.FINDINGTARGET)
        {
            StartCoroutine(SceneLoader.LoadSceneAsync(challengeAssociated, () =>
            {
                GameManager.Warning("Loading new Scene: " + challengeAssociated.ChallengName, "red");
                currentState = GameState.GAMEPLAY;
                foreach(Transform child in Instance.ChallengeTargetParent.transform)
                {
                    child.gameObject.SetActive(false);
                }
            }));
        }
    }

    public static void Warning(string message, string colorName)
    {
        Debug.Log("<color="+colorName+"> "+message+" " + "</color>");
    }

    IEnumerator LoadSceneAsync(string SceneName, Action Callback)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
        while(!asyncLoad.isDone)
        {
            yield return null;
        }
        Callback();
    }
}
