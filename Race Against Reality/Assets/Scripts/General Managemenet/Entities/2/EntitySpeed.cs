﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpeed : EntityPlatform
{
    private void OnCollisionStay(Collision collision)
    {
        if (Challenge.CurrentChallenge<Challenge_Platforms>().currentSpeed != Challenge.CurrentChallenge<Challenge_Platforms>().moverSpeed * 2)
        {
            Challenge.CurrentChallenge<Challenge_Platforms>().currentSpeed = Challenge.CurrentChallenge<Challenge_Platforms>().moverSpeed * 2;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(Challenge.CurrentChallenge<Challenge_Platforms>().currentSpeed == Challenge.CurrentChallenge<Challenge_Platforms>().moverSpeed * 2)
        {
            Challenge.CurrentChallenge<Challenge_Platforms>().currentSpeed = Challenge.CurrentChallenge<Challenge_Platforms>().moverSpeed;
        }
    }
}
