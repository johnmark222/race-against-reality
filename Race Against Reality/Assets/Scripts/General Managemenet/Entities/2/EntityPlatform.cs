﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EntityPlatform : EntityItem, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    [SerializeField]
    Target_EnableFollow followedObject;
    public bool IsDragging;
    public void OnBeginDrag(PointerEventData eventData)
    {
        IsDragging = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("S");

        transform.position = eventData.position;

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        IsDragging = false;
    }
}
