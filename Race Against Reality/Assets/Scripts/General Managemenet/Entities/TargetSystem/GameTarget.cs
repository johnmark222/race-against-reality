﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTarget : TargetEntity
{
	private void Awake() {
		Initialize();	
	}
	public override void Initialize()
	{
		base.Initialize();
		if(currentTargetType != TargetType.GAME)currentTargetType = TargetType.GAME;
		currentCustomTrackerBehavior.OnTrackingFoundEvent.AddListener(LoadScene);
	}

    /// <summary>
    /// Loads the associated scene based on the current target
    /// </summary>
	[ContextMenu("Load Associated Scene of the target")]
	public void LoadScene()
	{
        if(GameManager.CurrentChallenge == null)
            GameManager.Instance.LoadChallenge(this.challengeAssociated);
	}

}
