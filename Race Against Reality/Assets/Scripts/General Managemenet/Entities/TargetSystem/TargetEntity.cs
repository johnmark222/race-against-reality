﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public enum TargetType{BADGE, ITEM, GAME}

[RequireComponent(typeof(ImageTargetBehaviour))]
[RequireComponent(typeof(BaseCustomTracker))]
public class TargetEntity : EntityItem
{
	protected ImageTargetBehaviour currentTargetBehavior;
	public ImageTargetBehaviour CurrentTargetBehavior{get{return currentTargetBehavior;}}
	protected BaseCustomTracker currentCustomTrackerBehavior;
	public BaseCustomTracker CurrentCustomTrackerBehavior{get{return currentCustomTrackerBehavior;}}

	[SerializeField] protected TargetType currentTargetType;
	public TargetType CurrentTargetType{get{return currentTargetType;}}
	[SerializeField] protected ChallengeObject challengeAssociated;
	public ChallengeObject ChallengeAssociated{get{return challengeAssociated;}}

	public override void Initialize()
	{
		base.Initialize();
		currentTargetBehavior = GetComponent<ImageTargetBehaviour>();
		currentCustomTrackerBehavior = GetComponent<BaseCustomTracker>();

		
	}

}
