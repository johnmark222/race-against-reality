﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityItem : MonoBehaviour 
{
	[Tooltip("The Object that this item is associated to. Usually the setting of the current object.")]
	[SerializeField]
	internal EntityObject EntityObject;
	
	public string EntityName{ get {return this.EntityObject.EntityName; } }
	public bool HasProperty(string propertyCompare)
	{
		return EntityObject.HasProperty(propertyCompare);
	}

	public virtual void Initialize()
	{
		
	}

	public virtual void Reset()
	{

	}

}
