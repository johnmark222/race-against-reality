﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
[RequireComponent(typeof(ImageTargetBehaviour))]
[RequireComponent(typeof(BaseCustomTracker))]
public class Target_EnableFollow : TargetEntity
{
    public GameObject ObjectConcerned;
    public Vector3 FollowAxis;
    [SerializeField] internal bool IsCopyingRotation;
    public Vector3 RotateAxis;
    public float minX, maxX, minY, maxY;
    public bool IsClampingPos;
    [SerializeField]
    Vector3 initialPosition;
    public bool IsFollowing = true;


    public override void Initialize()
    {
        base.Initialize();
        currentTargetType = TargetType.ITEM;
        currentCustomTrackerBehavior.OnTrackingFoundEvent.AddListener(FoundTargetInitialize);
        currentCustomTrackerBehavior.OnTrackingLostEvent.AddListener(LostTargetEvent);
        initialPosition = ObjectConcerned.transform.position;
    }

    private void Update()
    {

        if (!IsFollowing) return;

//#if UNITY_EDITOR
//        Vector3 newPosition = new Vector3(
//                ObjectConcerned.transform.position.x + Input.GetAxis("Horizontal"),
//                ObjectConcerned.transform.position.y + Input.GetAxis("Vertical"),
//                initialPosition.z);



//            if (!IsClampingPos) return;

//            if (newPosition.x < minX) newPosition.x = minX + initialPosition.x;
//            if (newPosition.x > maxX) newPosition.x = maxX + initialPosition.x;
//            if (newPosition.y < minY) newPosition.y = minY + initialPosition.y;
//            if (newPosition.y > maxY) newPosition.y = maxY + initialPosition.y;

//            ObjectConcerned.transform.position = newPosition;
        
//#else

        if (ObjectConcerned.activeSelf)
        {

            Vector3 newPosition = new Vector3(initialPosition.x + (transform.position.x * FollowAxis.x),
                initialPosition.y + (transform.position.y * FollowAxis.y),
                initialPosition.z + (transform.position.z * FollowAxis.z));



            if (IsClampingPos)
            {
                if (newPosition.x < minX) newPosition.x = initialPosition.x + minX;
                if (newPosition.x > maxX) newPosition.x = initialPosition.x + maxX;
                if (newPosition.y < minY) newPosition.y = initialPosition.y + minY;
                if (newPosition.y > maxY) newPosition.y = initialPosition.y + maxY;
            }
            if (IsCopyingRotation)
            {
                //Vector3 eulerFollow = 
                ObjectConcerned.transform.rotation = this.transform.rotation;
                ObjectConcerned.transform.eulerAngles = new Vector3(0, 0, ObjectConcerned.transform.eulerAngles.z);
            }
            ObjectConcerned.transform.position = newPosition;

        }
//#endif

    }

    public void FoundTargetInitialize()
    {
        ObjectConcerned.SetActive(true);
    }


    public void LostTargetEvent()
    {
        ObjectConcerned.SetActive(false);

    }
}
