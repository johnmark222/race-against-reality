﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBall : EntityItem
{

    private void OnCollisionEnter(Collision col)
    {

        EntityItem racket = col.gameObject.GetComponent<EntityItem>();
        if (!racket) return;

        if(racket.HasProperty("enemyracket"))
        {
            float x = HitFactor(transform.position, col.transform.position, col.collider.bounds.size.x);

            Vector2 dir = new Vector2(x, -1).normalized;

            GetComponent<Rigidbody>().velocity = dir * 20;
        }

        if(racket.HasProperty("frontracket"))
        {
            float x = HitFactor(transform.position, col.transform.position, col.collider.bounds.size.x);
            Vector2 dir = new Vector2(x, 1).normalized;

            GetComponent<Rigidbody>().velocity = dir * 20;
        }

        if(racket.HasProperty("leftracket"))
        {
            float y = HitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 dir = new Vector2(-1, y).normalized;
            GetComponent<Rigidbody>().velocity = dir * 20;
        }

        if (racket.HasProperty("rightracket"))
        {
            float y = HitFactor(transform.position, col.transform.position, col.collider.bounds.size.y);
            Vector2 dir = new Vector2(1, y).normalized;
            GetComponent<Rigidbody>().velocity = dir * 20;
        }

    }

    float HitFactor(Vector2 ballPas, Vector2 racketPos, float racketHeight)
    {
        return (ballPas.y - racketPos.y) / racketHeight;
    }
}
