﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScreen<T>
{
    CanvasGroup canvasGroup {get;set;}
    [SerializeField]
    Animator screenAnimator{get;set;}
    AudioClip clipOnEnable{get;set;}
}