﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "GroupChallenge", menuName = "Challenges/Group", order = 1)]
public class GroupChallengeObject : ScriptableObject
{
	[SerializeField]
	protected List<ChallengeObject> _challenges = new List<ChallengeObject>();
	
	[SerializeField]
	protected int _challengeCount;
	
	public int ChallengeCount
	{
		get
		{
			return _challengeCount;
		}
	}

	public void AddChallenge(ChallengeObject ChallengeToAdd)
	{
		if(_challenges.Contains(ChallengeToAdd)) return;

	}

	public ChallengeObject GetChallenge(string ChallengeName)
	{
		ChallengeObject cO = null;
		
		foreach(ChallengeObject c in _challenges)
		{
			if(c.ChallengName == ChallengeName) cO = c;
		}

		if(cO != null) return cO;
		else return null;
		
	}
}
