﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EntityObject", menuName = "Entity/Entity Object", order = 1)]
public class EntityObject : ScriptableObject
{
	public string EntityName;
	public List<EntityProperty> Properties;

	public bool HasProperty(string propertyCompare)
	{
		foreach(EntityProperty property in Properties)
		{
			if(property.PropertyMatch(propertyCompare))
			{
				return true;
			}
		}
		return false;
	}
}
