﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Challenge", menuName = "Challenges/Challenge", order = 2)]
public class ChallengeObject : ScriptableObject
{
	[SerializeField]
	internal string _challengeName;
	public string ChallengName { get {return _challengeName; } }
	
	[SerializeField]
	internal int _challenge_ID;
	public int ChallengeID{ get { return _challenge_ID; } }

	[SerializeField]
	internal string _challengeScore;
	public string ChallengeScoreString { get { return _challengeScore; } }
	public int ChallengeScore 
	{
		get 
		{
			int temp = 0;
			Int32.TryParse(_challengeScore, out temp);
			return temp; 
		}
        internal set
        {
            _challengeScore = value.ToString();
        }
	}
	public void SetChallengeScore(int NewScore)
	{
		_challengeScore = NewScore.ToString();			
	}

	[SerializeField]
	internal ChallengeCurrentState _currentChallengeState;

	public int GetChallengeState()
	{
		switch(_currentChallengeState)
		{
			case ChallengeCurrentState.NOTSTARTED:
				return 0;
			case ChallengeCurrentState.PLAYING:
				return 1;
			case ChallengeCurrentState.FINISHED:
				return 2;
			default:
				return 9999;
		}
	}

}

public enum ChallengeCurrentState
{
	NOTSTARTED,
	PLAYING,
	FINISHED,
}
