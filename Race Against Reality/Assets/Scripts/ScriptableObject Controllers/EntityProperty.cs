﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EntityProperty", menuName = "Entity/Entity Property", order = 2)]
public class EntityProperty : ScriptableObject 
{
    public string Property;

    public bool PropertyMatch(string propertyCompare)
    {
        if(Property == propertyCompare) return true;
        else return false;
    }
}
